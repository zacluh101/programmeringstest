﻿using System.Collections.Generic;
using System.Linq;

namespace Model.Visualizer
{
	public class Visualizer : IVisualizer
	{
		private const string Empty = "  ";
		private const string Room = "██";
		private const string Car = "--";

		public List<string> GetVisualOutput(List<Coordinate> roomCoordinates, List<Coordinate> carCoordinates)
		{
			var xMax = roomCoordinates.OrderByDescending(coordinate => coordinate.X).FirstOrDefault()?.X;
			var yMax = roomCoordinates.OrderByDescending(coordinate => coordinate.Y).FirstOrDefault()?.Y;
			var xMin = roomCoordinates.OrderBy(coordinate => coordinate.X).FirstOrDefault()?.X;
			var yMin = roomCoordinates.OrderBy(coordinate => coordinate.Y).FirstOrDefault()?.Y;
			var width = xMax - xMin + 1;
			var height = yMax - yMin + 1;

			var lines = new List<string>();
			for (var y = 0; y < height; y++)
			{
				// ReSharper disable once PossibleInvalidOperationException
				//var line = new char[(int)width];
				var line = new List<string>();
				for (var x = 0; x < width; x++)
				{
					var xOffset = x + xMin;
					var yOffset = y + yMin;
					var c = Empty;

					if (roomCoordinates.FirstOrDefault(coordinate => coordinate.X == xOffset && coordinate.Y == yOffset) != null)
					{
						c = Room;
					}
					if (carCoordinates.FirstOrDefault(coordinate => coordinate.X == xOffset && coordinate.Y == yOffset) != null)
					{
						c = Car;
					}
					line.Add(c);
				}
				lines.Add(string.Concat(line));
			}
			lines.Reverse();
			lines.Add(string.Empty);
			lines.Add($"{Room} = room");
			lines.Add($"{Car} = car path");
			return lines;
		}
	}
}