﻿using System.Collections.Generic;

namespace Model.Visualizer
{
	public interface IVisualizer
	{
		List<string> GetVisualOutput(List<Coordinate> roomCoordinates, List<Coordinate> carCoordinates);
	}
}
