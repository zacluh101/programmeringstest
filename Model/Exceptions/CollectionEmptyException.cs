﻿using System;

namespace Model.Exceptions
{
	public class CollectionEmptyException : Exception, IException
	{
	}
}