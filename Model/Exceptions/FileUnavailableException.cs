﻿using System;

namespace Model.Exceptions
{
	public class FileUnavailableException : Exception, IException
	{
	}
}