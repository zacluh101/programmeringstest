﻿using System;

namespace Model.Exceptions
{
	public class InvalidCommandException : Exception, IException
	{
	}
}