﻿using System;

namespace Model.Exceptions
{
	public class ReaderException : Exception, IException
	{
		public ReaderException(Type type)
			: base($"Input of {type} exceeded maximum retries")
		{
		}
	}
}