﻿using System;

namespace Model
{
	public class Coordinate
	{
		public int X { get; set; }
		public int Y { get; set; }

		public Coordinate(int x, int y)
		{
			X = x;
			Y = y;
		}

		public Coordinate(DoubleCoordinate doubleCoordinate)
		{
			X = (int)Math.Round(doubleCoordinate.X, 0, MidpointRounding.AwayFromZero);
			Y = (int)Math.Round(doubleCoordinate.Y, 0, MidpointRounding.AwayFromZero);
		}

		public override string ToString()
		{
			return $"({X},{Y})";
		}
	}
}