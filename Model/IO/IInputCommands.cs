﻿using System.Collections.Generic;

namespace Model.IO
{
	public interface IInputCommands
	{
		List<char> InputComs();
	}
}