﻿using Model.IO.IoHandler;

namespace Model.IO
{
	public class InputHandler : IInputHandler
	{
		private readonly IInputRoom _inputRoom;
		private readonly IInputStartingPoint _inputStartingPoint;
		private readonly IInputCommands _inputCommands;
		private readonly IIoHandler _ioHandler;

		public InputHandler(IInputRoom inputRoom,
			IInputStartingPoint inputStartingPoint,
			IInputCommands inputCommands,
			IIoHandler ioHandler)
		{
			_inputRoom = inputRoom;
			_inputStartingPoint = inputStartingPoint;
			_inputCommands = inputCommands;
			_ioHandler = ioHandler;
		}

		public IInputValues GetInputValues()
		{
			var room = _inputRoom.InputRoom();
			_ioHandler.WriteLine("");
			var startingPoint = _inputStartingPoint.InputPoint();
			_ioHandler.WriteLine("");
			var commands = _inputCommands.InputComs();
			_ioHandler.WriteLine("");
			return new InputValues
			{
				Room = room,
				StartingPoint = startingPoint,
				Commands = commands
			};
		}
	}
}