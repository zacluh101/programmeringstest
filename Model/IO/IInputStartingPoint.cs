﻿namespace Model.IO
{
	public interface IInputStartingPoint
	{
		StartingPoint InputPoint();
	}
}