﻿using System;
using System.Collections.Generic;
using Model.Exceptions;
using Model.IO.IoHandler;
using Model.IO.Parser;

namespace Model.IO
{
	public class InputStartingPoint : IInputStartingPoint
	{
		private readonly IIoHandler _ioHandler;
		private readonly IParser _parser;

		public InputStartingPoint(IIoHandler ioHandler, IParser parser)
		{
			_ioHandler = ioHandler;
			_parser = parser;
		}

		public StartingPoint InputPoint()
		{
			var retryCount = 0;
			List<object> dimensions;
			bool parseResult;
			do
			{
				_ioHandler.WriteLine("Input starting point as: [X] [Y] [Direction(N/S/W/E)]");
				var inp = _ioHandler.Read();
				parseResult = _parser.ParseTokens(inp, new List<Type> { typeof(int), typeof(int), typeof(Direction) }, out dimensions);
				if (parseResult == false)
					_ioHandler.WriteLine("Error in input, try again");
				retryCount++;
				if (retryCount > 10)
					throw new ReaderException(GetType());
			} while (parseResult == false);
			var direction = _parser.ParseDirection((Direction)dimensions[2]);
			return new StartingPoint
			{
				Coordinate = new Coordinate((int)dimensions[0], (int)dimensions[1]),
				Direction = direction
			};
		}
	}
}