﻿using System.Collections.Generic;
using Model.Room;

namespace Model.IO
{
	public class InputValues : IInputValues
	{
		public List<char> Commands { get; set; }
		public IRoom Room { get; set; }
		public StartingPoint StartingPoint { get; set; }
	}
}