﻿namespace Model.IO
{
	public interface IInputHandler
	{
		IInputValues GetInputValues();
	}
}