﻿using System;
using System.Collections.Generic;
using Model.Exceptions;
using Model.IO.IoHandler;
using Model.IO.Parser;
using Model.Room;

namespace Model.IO
{
	public class InputSquareRoom : IInputRoom
	{
		private readonly IIoHandler _ioHandler;
		private readonly IParser _parser;

		public InputSquareRoom(IIoHandler ioHandler, IParser parser)
		{
			_ioHandler = ioHandler;
			_parser = parser;
		}

		public IRoom InputRoom()
		{
			var retryCount = 0;
			List<object> dimensions;
			bool parseResult;
			do
			{
				_ioHandler.WriteLine("Input square room dimensions as: [W] [H]");
				var inp = _ioHandler.Read();
				parseResult = _parser.ParseTokens(inp, new List<Type> { typeof(int), typeof(int) }, out dimensions);
				if (parseResult == false)
					Console.WriteLine("Error in input, try again");
				retryCount++;
				if (retryCount > 10)
					throw new ReaderException(GetType());
			} while (parseResult == false);
			return new SquareRoom((int)dimensions[0], (int)dimensions[1]);
		}
	}
}