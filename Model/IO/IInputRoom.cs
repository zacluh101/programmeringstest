﻿using Model.Room;

namespace Model.IO
{
	public interface IInputRoom
	{
		IRoom InputRoom();
	}
}