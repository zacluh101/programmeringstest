﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Model.IO.Parser
{
	public class Parser : IParser
	{
		public bool ParseRoute(string input, List<char> validValues, out List<char> keyCommands)
		{
			keyCommands = new List<char>();
			foreach (var c in input.ToCharArray())
			{
				if (c == ' ')
					continue;
				var lower = char.ToLower(c);
				if (!validValues.Contains(lower))
					return false;
				keyCommands.Add(lower);
			}
			return true;
		}

		public bool ParseTokens(string tokenString, List<Type> types, out List<object> values)
		{
			var tokens = tokenString.Trim().Split(' ').ToList();
			values = new List<object>();
			if (tokens.Count != types.Count)
				return false;
			for (var index = 0; index < tokens.Count; index++)
			{
				object output;
				if (!TryCastValue(types[index], tokens[index], out output))
					return false;
				values.Add(output);
			}
			return true;
		}

		public double ParseDirection(Direction direction)
		{
			switch (direction)
			{
				case Direction.N:
					return Math.PI / 2;
				case Direction.E:
					return 0;
				case Direction.S:
					return 3 * Math.PI / 2;
				case Direction.W:
					return Math.PI;
				default:
					throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
			}

		}

		private static bool TryCastValue(Type type, string value, out object output)
		{
			output = null;
			if (type == null)
			{
				output = string.IsNullOrEmpty(value) ? null : value;
				return true;
			}
			try
			{
				if (type.IsEnum)
				{
					if (Enum.IsDefined(type, value))
					{
						output = Enum.Parse(type, value);
						return true;
					}
					return false;
				}
				var converter = TypeDescriptor.GetConverter(type);
				if (!converter.CanConvertFrom(typeof(string)))
					return false;
				output = converter.ConvertFrom(value);
				return true;
			}
			catch (Exception e)
			{
				if (e is NotSupportedException || e is FormatException
					|| e.InnerException?.ToString() == "System.FormatException: Input string was not in a correct format.\r\n   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)\r\n   at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)\r\n   at System.ComponentModel.Int32Converter.FromString(String value, NumberFormatInfo formatInfo)\r\n   at System.ComponentModel.BaseNumberConverter.ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, Object value)") // Bad coding - but what to do when un-documented exception is thrown?
					return false;
				throw;
			}
		}
	}
}