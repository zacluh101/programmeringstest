﻿using System;
using System.Collections.Generic;

namespace Model.IO.Parser
{
	public interface IParser
	{
		bool ParseRoute(string input, List<char> validValues, out List<char> keyCommands);
		bool ParseTokens(string tokenString, List<Type> types, out List<object> values);
		double ParseDirection(Direction direction);
	}
}