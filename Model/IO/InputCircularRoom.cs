﻿using System;
using System.Collections.Generic;
using Model.Exceptions;
using Model.IO.IoHandler;
using Model.IO.Parser;
using Model.Room;

namespace Model.IO
{
	public class InputCircularRoom : IInputRoom
	{
		private readonly IIoHandler _ioHandler;
		private readonly IParser _parser;

		public InputCircularRoom(IIoHandler ioHandler, IParser parser)
		{
			_ioHandler = ioHandler;
			_parser = parser;
		}

		public IRoom InputRoom()
		{
			var retryCount = 0;
			List<object> dimensions;
			bool parseResult;
			do
			{
				_ioHandler.WriteLine("Input circular room dimensions as: [R(=radius)]");
				var inp = _ioHandler.Read();
				parseResult = _parser.ParseTokens(inp, new List<Type> { typeof(int) }, out dimensions);
				if (parseResult == false
					|| ((int)dimensions[0] < 1))
					Console.WriteLine("Error in input, try again");
				retryCount++;
				if (retryCount > 10)
					throw new ReaderException(GetType());
			} while (parseResult == false);
			return new CircularRoom((int)dimensions[0]);
		}
	}
}