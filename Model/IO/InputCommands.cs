﻿using System;
using System.Collections.Generic;
using Model.Exceptions;
using Model.IO.IoHandler;
using Model.IO.Parser;

namespace Model.IO
{
	public class InputCommands : IInputCommands
	{
		private readonly IIoHandler _ioHandler;
		private readonly IParser _parser;

		public InputCommands(IIoHandler ioHandler, IParser parser)
		{
			_ioHandler = ioHandler;
			_parser = parser;
		}

		public List<char> InputComs()
		{
			var retryCount = 0;
			List<char> commands;
			bool parseResult;
			do
			{
				_ioHandler.WriteLine("Input room commands as: [Commands(F/B/R/L)]");
				var inp = _ioHandler.Read();
				parseResult = _parser.ParseRoute(inp, new List<char> { 'f', 'b', 'r', 'l' }, out commands);
				if (parseResult == false)
					Console.WriteLine("Error in input, try again");
				retryCount++;
				if (retryCount > 10)
					throw new ReaderException(GetType());
			} while (parseResult == false);
			return commands;
		}
	}
}