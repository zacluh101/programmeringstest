﻿using System.Collections.Generic;
using Model.Room;

namespace Model.IO
{
	public interface IInputValues
	{
		List<char> Commands { get; set; }
		IRoom Room { get; set; }
		StartingPoint StartingPoint { get; set; }
	}
}