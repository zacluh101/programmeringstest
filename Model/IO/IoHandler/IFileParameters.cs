﻿namespace Model.IO.IoHandler
{
	public interface IFileParameters
	{
		string FileName { get; set; }
	}
}