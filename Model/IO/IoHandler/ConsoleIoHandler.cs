﻿using System;

namespace Model.IO.IoHandler
{
	public class ConsoleIoHandler : IIoHandler
	{
		public string Read()
		{
			return Console.ReadLine();
		}

		public void WriteLine(string text)
		{
			Console.WriteLine(text);
		}
	}
}