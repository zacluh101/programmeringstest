﻿namespace Model.IO.IoHandler
{
	public class FileParameters : IFileParameters
	{
		public string FileName { get; set; }
	}
}