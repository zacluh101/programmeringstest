﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using Model.Exceptions;

namespace Model.IO.IoHandler
{
	public class FileIoHandler : IIoHandler
	{
		private readonly List<string> _lines;

		public FileIoHandler(IFileParameters fileParameters)
		{
			if (!File.Exists(fileParameters.FileName))
				throw new FileUnavailableException();
			try
			{
				_lines = File.ReadAllLines(fileParameters.FileName).ToList();

			}
			catch (Exception e)
			{
				if (e is ArgumentException
					|| e is PathTooLongException
					|| e is IOException
					|| e is UnauthorizedAccessException
					|| e is NotSupportedException
					|| e is SecurityException)
					throw new FileUnavailableException();
				throw;
			}
		}

		public string Read()
		{
			if (_lines.Count == 0)
				throw new CollectionEmptyException();
			var line = _lines.First();
			_lines.RemoveAt(0);
			return line;
		}

		public void WriteLine(string text)
		{
			Console.WriteLine(text);
		}
	}
}
