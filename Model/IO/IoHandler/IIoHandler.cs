﻿namespace Model.IO.IoHandler
{
	public interface IIoHandler
	{
		string Read();
		void WriteLine(string text);
	}
}