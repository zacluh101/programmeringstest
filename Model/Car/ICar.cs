﻿namespace Model.Car
{
	public interface ICar
	{
		void Initialize(StartingPoint startingPoint);
		void ProcessCommand(char command);
		Coordinate GetCoordinate();
	}
}