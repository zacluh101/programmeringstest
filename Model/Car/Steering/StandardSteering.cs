﻿using System;
using Model.Exceptions;

namespace Model.Car.Steering
{
	public class StandardSteering : ISteering
	{
		public double ProcessCommand(double direction, char command)
		{
			switch (command)
			{
				case 'l':
					return (direction + Math.PI / 2) % (Math.PI * 2);
				case 'r':
					return (direction - Math.PI / 2) % (Math.PI * 2);
				default:
					throw new InvalidCommandException();
			}
		}
	}
}