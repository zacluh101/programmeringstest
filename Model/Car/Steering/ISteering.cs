﻿namespace Model.Car.Steering
{
	public interface ISteering
	{
		double ProcessCommand(double direction, char command);
	}
}