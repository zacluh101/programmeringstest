﻿namespace Model.Car.Gearbox
{
	public interface IGearbox
	{
		double GetTransmission(char command);
	}
}