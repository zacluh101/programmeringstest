﻿using System;

namespace Model.Car.Gearbox
{
	public class StandardGearbox : IGearbox
	{
		public double GetTransmission(char command)
		{
			switch (command)
			{
				case 'f':
					return 1;
				case 'b':
					return -1;
				default:
					throw new NotImplementedException();
			}
		}
	}
}