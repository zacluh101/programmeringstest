﻿using System;
using Model.Car.Engine;
using Model.Car.Gearbox;
using Model.Car.Steering;

namespace Model.Car
{
	public class Car : ICar
	{
		private readonly IEngine _engine;
		private readonly ISteering _steering;
		private readonly IGearbox _gearbox;
		private DoubleCoordinate _currentCoordinate;
		private double _currentDirection;

		public Car(IEngine engine, ISteering steering, IGearbox gearbox)
		{
			_engine = engine;
			_steering = steering;
			_gearbox = gearbox;
		}

		public void Initialize(StartingPoint startingPoint)
		{
			_currentCoordinate = new DoubleCoordinate(startingPoint.Coordinate.X, startingPoint.Coordinate.Y);
			_currentDirection = startingPoint.Direction;
		}

		public void ProcessCommand(char command)
		{
			switch (command)
			{
				case 'l':
				case 'r':
					_currentDirection = _steering.ProcessCommand(_currentDirection, command);
					break;
				case 'b':
				case 'f':
					_currentCoordinate.Add(GetOffset(command));
					break;
			}
		}

		private DoubleCoordinate GetOffset(char command)
		{
			var distance = _engine.GetTorque() * _gearbox.GetTransmission(command);
			var x = Math.Cos(_currentDirection) * distance;
			var y = Math.Sin(_currentDirection) * distance;
			return new DoubleCoordinate(x, y);
		}

		public Coordinate GetCoordinate()
		{
			return new Coordinate(_currentCoordinate);
		}
	}
}