﻿namespace Model.Car.Engine
{
	public interface IEngine
	{
		double GetTorque();
	}
}