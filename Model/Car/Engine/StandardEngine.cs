﻿namespace Model.Car.Engine
{
	public class StandardEngine : IEngine
	{
		public double GetTorque()
		{
			return 1;
		}
	}
}