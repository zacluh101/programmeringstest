﻿using System.Collections.Generic;
using System.Linq;
using Model.Car;
using Model.IO;
using Model.IO.IoHandler;
using Model.Room;
using Model.Visualizer;

namespace Model.TestHandler
{
	public class TestRunner : ITestRunner
	{
		private readonly ICar _car;
		private readonly IIoHandler _ioHandler;
		private readonly IVisualizer _visualizer;


		public TestRunner(ICar car, IIoHandler ioHandler, IVisualizer visualizer)
		{
			_car = car;
			_ioHandler = ioHandler;
			_visualizer = visualizer;
		}

		public void RunTest(IInputValues values)
		{
			_car.Initialize(values.StartingPoint);
			List<Coordinate> log;
			if (Runner(values.Room, values.Commands, out log))
			{
				_ioHandler.WriteLine("Test successfully passed");
			}
			else
			{
				_ioHandler.WriteLine("Test failed");
				var crashCoordinate = log.LastOrDefault();
				_ioHandler.WriteLine($"Car crashed at coordinate {crashCoordinate}");
			}

			_ioHandler.WriteLine($"Full log: {string.Join(", ", log)})");
			_ioHandler.WriteLine("");
			var visual = _visualizer.GetVisualOutput(values.Room.GetRoomCoordinates(), log);
			foreach (var output in visual)
			{
				_ioHandler.WriteLine(output);
			}
		}

		private bool Runner(IRoom room, IEnumerable<char> commands, out List<Coordinate> log)
		{
			log = new List<Coordinate> { _car.GetCoordinate() };
			if (!room.IsInRoom(log.First()))
				return false; // if start-coordinate is outside room

			foreach (var command in commands)
			{
				_car.ProcessCommand(command);
				var currentCoordinate = _car.GetCoordinate();
				log.Add(currentCoordinate);
				if (!room.IsInRoom(currentCoordinate))
				{
					return false;
				}
			}
			return true;
		}
	}
}
