﻿using Model.IO;

namespace Model.TestHandler
{
	public interface ITestRunner
	{
		void RunTest(IInputValues values);
	}
}