﻿namespace Model
{
	public class DoubleCoordinate
	{
		public double X { get; set; }
		public double Y { get; set; }

		public DoubleCoordinate(double x, double y)
		{
			X = x;
			Y = y;
		}

		public DoubleCoordinate(Coordinate coordinate)
		{
			X = coordinate.X;
			Y = coordinate.Y;
		}


		public void Add(DoubleCoordinate coordinate)
		{
			X = X + coordinate.X;
			Y = Y + coordinate.Y;
		}
	}
}