﻿namespace Model
{
	public class StartingPoint
	{
		public Coordinate Coordinate { get; set; }
		public double Direction { get; set; }
	}
}