﻿using System.Collections.Generic;

namespace Model.Room
{
	public class SquareRoom : IRoom
	{
		public int Width { get; set; }
		public int Height { get; set; }

		public SquareRoom(int width, int height)
		{
			Width = width;
			Height = height;
		}

		public bool IsInRoom(Coordinate coordinate)
		{
			return coordinate.X >= 0
				   && coordinate.X < Width
				   && coordinate.Y >= 0
				   && coordinate.Y < Height;
		}

		public List<Coordinate> GetRoomCoordinates()
		{
			var coordinates = new List<Coordinate>();
			for (var x = 0; x < Width; x++)
			{
				for (var y = 0; y < Height; y++)
				{
					coordinates.Add(new Coordinate(x, y));
				}
			}
			return coordinates;
		}
	}
}