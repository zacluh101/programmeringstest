﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Room
{
	public class CircularRoom : IRoom
	{
		private readonly List<Coordinate> _roomCoordinates;

		public CircularRoom(int radius)
		{
			_roomCoordinates = new List<Coordinate>();
			CreateRoomCoordinates(radius);
		}

		private void CreateRoomCoordinates(int radius)
		{
			var centerPoint = new Coordinate(radius, radius);
			for (var y = 0; y <= centerPoint.Y + radius; y++)
			{
				for (var x = 0; x <= centerPoint.X + radius; x++)
				{
					var coordinate = new Coordinate(x, y);
					if (IsInCircle(coordinate, centerPoint, radius))
						_roomCoordinates.Add(coordinate);
				}
			}
		}

		private static bool IsInCircle(Coordinate coordinate, Coordinate centerPoint, int radius)
		{
			var distance = Math.Sqrt(Math.Pow(coordinate.X - centerPoint.X, 2) +
									 Math.Pow(coordinate.Y - centerPoint.Y, 2));
			return distance <= radius;
		}

		public bool IsInRoom(Coordinate coordinate)
		{
			return
				_roomCoordinates.FirstOrDefault(
					roomCoordinate => roomCoordinate.X == coordinate.X && roomCoordinate.Y == coordinate.Y) != null;
		}

		public List<Coordinate> GetRoomCoordinates()
		{
			return _roomCoordinates;
		}
	}
}