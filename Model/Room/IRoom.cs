﻿using System.Collections.Generic;

namespace Model.Room
{
	public interface IRoom
	{
		bool IsInRoom(Coordinate coordinate);
		List<Coordinate> GetRoomCoordinates();
	}
}