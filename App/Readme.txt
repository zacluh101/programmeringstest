﻿Denna lösning bygger på antagandet att bilen som testas alltid är en och samma, men att den kan bestå av olika utbytbara delar.

De delar som är utbytbara på bilen i denna lösning är:
======================================================

Motor - IMotor
	specificerar hur stor motorkraft som ges på ett f/b-kommando. Implementationen "Standardmotor" ger 1 (double)

Växellåda - IGearBox
	specificerar hur utväxlingen är på ett l/r-kommando. Implementationen "StandardGearBox" ger 1 för fram och -1 för bak (double)

Styrning - ISteering
	specificerar hur mycket bilen svänger på ett l/r-kommando. Implementationen "StandardSteering" ger +PI/2 för l och -PI/2 för r (double)


	
Rummet som bilen kör i är också anpassningsbart
===============================================

Rum - IRoom
	kan se ut i princip hur som helst, måste kunna svara på metoderna IsInRoom(Coordinate) och GetCoordinates(), den sistnämnda ger alla rumskoordinater och används till utskrift.
	


Hjälpinterface som kan kommenteras
==================================

IInputRoom:
Till ett nytt IRoom skapas med fördel en implementation av IInputRoom, och som då används för att mata in parametrarna till det rummet. Ett SquareRoom tar ju width samt height, medan ett CircularRoom enbart tar radius.

IIoHandler:
IIoHandler är den som används för att bestämma hur inmatningen går till. Implementationen "ConsoleIoHandler" används för att mata in data manuellt. Med hjälp av implementationen "FileIoHandler" finns möjlighet att istället specificera en textfil med kommandon som ska läsas in.



Dependency Injection
====================

Lösningen använder sig av DryIoc som bibliotek för att hantera DI. I klassen App.Program specificeras vilka implementationer som ska användas vid körningen.