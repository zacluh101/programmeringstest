﻿using System;
using DryIoc;
using Model.Car;
using Model.Car.Engine;
using Model.Car.Gearbox;
using Model.Car.Steering;
using Model.Exceptions;
using Model.IO;
using Model.IO.IoHandler;
using Model.IO.Parser;
using Model.TestHandler;
using Model.Visualizer;

namespace App
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var container = new Container();

			/* Only one of the following should be active */
			/* ---------------------------------------------- */
			RegisterConsoleInputWithSquareRoom(container);
			//RegisterSquareRoomDemo(container);
			//RegisterCircularRoomDemo(container);
			/* ---------------------------------------------- */


			RegisterCarProperties(container);

			RegisterDefaultTypes(container);
			RunApplication(container);
		}

		private static void RunApplication(IResolver container)
		{
			try
			{
				var inputHandler = container.Resolve<IInputHandler>();
				var values = inputHandler.GetInputValues();
				var testRunner = container.Resolve<ITestRunner>();
				testRunner.RunTest(values);
			}
			catch (Exception e)
			{
				if (e is IException)
					Console.WriteLine($"Handled exception: {e.Message}");
				else
					Console.WriteLine($"Unhandled exception: {e.Message}");
			}
			Console.ReadKey();
		}

		private static void RegisterCarProperties(IRegistrator container)
		{
			container.Register<ISteering, StandardSteering>();
			container.Register<IEngine, StandardEngine>();
			container.Register<IGearbox, StandardGearbox>();
		}

		private static void RegisterCircularRoomDemo(IRegistrator container)
		{
			const string fileName = "testdata_circularroom.txt";
			container.Register<IInputRoom, InputCircularRoom>();
			container.Register<IFileParameters, FileParameters>(made:
				Made.Of(() => new FileParameters { FileName = fileName }));
			container.Register<IIoHandler, FileIoHandler>(Reuse.Singleton);
		}

		private static void RegisterSquareRoomDemo(IRegistrator container)
		{
			const string fileName = "testdata_squareroom.txt";
			container.Register<IInputRoom, InputSquareRoom>();
			container.Register<IFileParameters, FileParameters>(made:
				Made.Of(() => new FileParameters { FileName = fileName }));
			container.Register<IIoHandler, FileIoHandler>(Reuse.Singleton);
		}

		private static void RegisterConsoleInputWithSquareRoom(IRegistrator container)
		{
			container.Register<IIoHandler, ConsoleIoHandler>();
			container.Register<IInputRoom, InputSquareRoom>();
		}

		private static void RegisterDefaultTypes(IRegistrator container)
		{
			container.Register<IParser, Parser>();
			container.Register<IInputStartingPoint, InputStartingPoint>();
			container.Register<IInputCommands, InputCommands>();
			container.Register<ITestRunner, TestRunner>();
			container.Register<IVisualizer, Visualizer>();
			container.Register<IInputHandler, InputHandler>();
			container.Register<ICar, Car>();
		}
	}
}
