﻿using Model;
using Model.Room;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class SquareRoomTest
	{
		private SquareRoom _room;

		[SetUp]
		public void Setup()
		{
			_room = new SquareRoom(3, 6);
		}

		[Test]
		public void Test()
		{
			Assert.IsTrue(_room.IsInRoom(new Coordinate(0, 0)));
			Assert.IsTrue(_room.IsInRoom(new Coordinate(2, 0)));
			Assert.IsFalse(_room.IsInRoom(new Coordinate(3, 0)));
			Assert.IsTrue(_room.IsInRoom(new Coordinate(2, 5)));
			Assert.IsFalse(_room.IsInRoom(new Coordinate(2, 6)));
			Assert.IsFalse(_room.IsInRoom(new Coordinate(0, 6)));
			Assert.IsFalse(_room.IsInRoom(new Coordinate(0, -1)));
			Assert.IsFalse(_room.IsInRoom(new Coordinate(-1, 0)));
		}
	}
}
