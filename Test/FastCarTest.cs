﻿using System;
using DryIoc;
using Model;
using Model.Car;
using Model.Car.Engine;
using Model.Car.Gearbox;
using Model.Car.Steering;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class FastCarTest
	{
		private ICar _car;

		[SetUp]
		public void Setup()
		{
			var container = new Container();
			container.Register<ICar, Car>();
			container.Register<IEngine, FastEngine>();
			container.Register<IGearbox, StandardGearbox>();
			container.Register<ISteering, StandardSteering>();

			_car = container.Resolve<ICar>();
			var startingPoint = new StartingPoint
			{
				Coordinate = new Coordinate(0, 0),
				Direction = Math.PI / 2
			};
			_car.Initialize(startingPoint);
		}

		[Test]
		public void Test()
		{
			_car.ProcessCommand('f');
			var coordinate = _car.GetCoordinate();
			Assert.AreEqual(coordinate.X, 0);
			Assert.AreEqual(coordinate.Y, 2);
		}
	}
}