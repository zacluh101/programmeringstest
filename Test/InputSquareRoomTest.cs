﻿using System.Diagnostics;
using DryIoc;
using Model.IO;
using Model.IO.IoHandler;
using Model.IO.Parser;
using Model.Room;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class InputSquareRoomTest
	{
		private IInputRoom _inputRoom;

		[SetUp]
		public void Setup()
		{
			var container = new Container();
			container.Register<IInputRoom, InputSquareRoom>();
			container.Register<IParser, Parser>();
			container.Register<IIoHandler, SquareRoomIoHandler>();

			_inputRoom = container.Resolve<IInputRoom>();
		}

		[Test]
		public void Test()
		{
			var room = _inputRoom.InputRoom();
			Assert.That(() => room is SquareRoom);
			var squareRoom = room as SquareRoom;
			Debug.Assert(squareRoom != null, "squareRoom != null");
			Assert.AreEqual(squareRoom.Width, 8);
			Assert.AreEqual(squareRoom.Height, 6);
		}
	}

	public class SquareRoomIoHandler : IIoHandler
	{
		public string Read()
		{
			return "8 6";
		}

		public void WriteLine(string text)
		{
		}
	}
}