﻿using System;
using DryIoc;
using Model;
using Model.Car;
using Model.Car.Engine;
using Model.Car.Gearbox;
using Model.Car.Steering;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class StandardCarTest
	{
		private ICar _car;

		[SetUp]
		public void Setup()
		{
			var container = new Container();
			container.Register<ICar, Car>();
			container.Register<IEngine, StandardEngine>();
			container.Register<IGearbox, StandardGearbox>();
			container.Register<ISteering, StandardSteering>();

			_car = container.Resolve<ICar>();
			var startingPoint = new StartingPoint
			{
				Coordinate = new Coordinate(3, -2),
				Direction = Math.PI / 2
			};
			_car.Initialize(startingPoint);
		}

		[Test]
		public void Test()
		{
			_car.ProcessCommand('f');
			var coordinate = _car.GetCoordinate();
			Assert.AreEqual(coordinate.X, 3);
			Assert.AreEqual(coordinate.Y, -1);

			_car.ProcessCommand('b');
			coordinate = _car.GetCoordinate();
			Assert.AreEqual(coordinate.X, 3);
			Assert.AreEqual(coordinate.Y, -2);

			_car.ProcessCommand('l');
			_car.ProcessCommand('f');
			_car.ProcessCommand('f');
			coordinate = _car.GetCoordinate();
			Assert.AreEqual(coordinate.X, 1);
			Assert.AreEqual(coordinate.Y, -2);

			_car.ProcessCommand('r');
			_car.ProcessCommand('b');
			_car.ProcessCommand('b');
			_car.ProcessCommand('l');
			_car.ProcessCommand('f');
			_car.ProcessCommand('l');
			coordinate = _car.GetCoordinate();
			Assert.AreEqual(coordinate.X, 0);
			Assert.AreEqual(coordinate.Y, -4);
		}
	}
}