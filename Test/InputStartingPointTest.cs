﻿using System;
using DryIoc;
using Model.IO;
using Model.IO.IoHandler;
using Model.IO.Parser;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class InputStartingPointTest
	{
		private IInputStartingPoint _inputStartingPoint;

		[SetUp]
		public void Setup()
		{
			var container = new Container();
			container.Register<IInputStartingPoint, InputStartingPoint>();
			container.Register<IParser, Parser>();
			container.Register<IIoHandler, StartingPointIoHandler>();

			_inputStartingPoint = container.Resolve<IInputStartingPoint>();
		}

		[Test]
		public void Test()
		{
			var point = _inputStartingPoint.InputPoint();
			Assert.AreEqual(point.Coordinate.X, 4);
			Assert.AreEqual(point.Coordinate.Y, 8);
			Assert.AreEqual(point.Direction, 3 * Math.PI / 2);
		}
	}

	public class StartingPointIoHandler : IIoHandler
	{
		public string Read()
		{
			return "4 8 S";
		}

		public void WriteLine(string text)
		{
		}
	}
}