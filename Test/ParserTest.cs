﻿using System;
using System.Collections.Generic;
using Model.IO.Parser;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class ParserTest
	{
		private IParser _parser;

		[SetUp]
		public void Setup()
		{
			_parser = new Parser();
		}

		[Test]
		public void TestRouteParser()
		{
			List<char> commands;
			var validChars = new List<char> { 'a', 'b', 'c' };

			var result = _parser.ParseRoute("abc", validChars, out commands);
			Assert.AreEqual(commands, new List<char> { 'a', 'b', 'c' });
			Assert.AreEqual(result, true);

			result = _parser.ParseRoute("a b C c a Bb", validChars, out commands);
			Assert.AreEqual(commands, new List<char> { 'a', 'b', 'c', 'c', 'a', 'b', 'b' });
			Assert.AreEqual(result, true);

			result = _parser.ParseRoute("abd", validChars, out commands);
			Assert.AreEqual(result, false);
		}

		[Test]
		public void TestTokenParser()
		{
			var types = new List<Type> { typeof(int), typeof(char), typeof(double) };
			List<object> output;
			var input = "23 g 42";

			var result = _parser.ParseTokens(input, types, out output);
			Assert.AreEqual(result, true);
			Assert.AreEqual(output[0], 23);
			Assert.AreEqual(output[1], 'g');
			Assert.AreEqual(output[2], 42.0d);

			input = "d2";
			result = _parser.ParseTokens(input, new List<Type> { typeof(int) }, out output);
			Assert.IsFalse(result);
		}
	}
}