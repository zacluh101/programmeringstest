﻿using System.Collections.Generic;
using DryIoc;
using Model.IO;
using Model.IO.IoHandler;
using Model.IO.Parser;
using NUnit.Framework;

namespace Test
{
	[TestFixture]
	public class InputCommandsTest
	{
		private IInputCommands _inputCommands;

		[SetUp]
		public void Setup()
		{
			var container = new Container();
			container.Register<IInputCommands, InputCommands>();
			container.Register<IParser, Parser>();
			container.Register<IIoHandler, CommandsIoHandler>();

			_inputCommands = container.Resolve<IInputCommands>();
		}

		[Test]
		public void Test()
		{
			var commands = _inputCommands.InputComs();
			Assert.AreEqual(commands, new List<char> { 'f', 'l', 'r', 'r', 'l', 'b', 'r' });
		}
	}

	public class CommandsIoHandler : IIoHandler
	{
		public string Read()
		{
			return "F L R rlb R";
		}

		public void WriteLine(string text)
		{
		}
	}
}